<?php namespace App\Helpers;

use GuzzleHttp;

class OneSignal
{

    public $app_id;
    public $api_url_player;
    public $api_url_notification;
    public $api_api_key;
    public $guzzle_http;

    /**
     * @desc Construct
     * */
    public function __construct()
    {
        if (strpos($_SERVER['HTTP_HOST'], 'dev') !== false || strpos($_SERVER['HTTP_HOST'], 'localhost') !== false) {
            $this->app_id = env('ONESIGNAL_DEV_APP_ID');
            $this->api_api_key = env('ONESIGNAL_DEV_API_KEY');
        }else{
            $this->app_id = env('ONESIGNAL_APP_ID');
            $this->api_api_key = env('ONESIGNAL_API_KEY');
        }
        $this->api_url_player = env('ONESIGNAL_API_URL_PLAYER');
        $this->api_url_notification = env('ONESIGNAL_API_URL_NOTIFICATION');
        $this->guzzle_http = new GuzzleHttp\Client();
    }

    /**
     * @desc add device
     *
     * @param string $device_type
     * @param string $device_token
     *
     * @return mixed
     * */
    public function addDevice($device_type, $device_token)
    {
        try{
            switch ($device_type) {
                case 0:
                    $device_type_to_number = 0;
                    break;
                case 1:
                    $device_type_to_number = 1;
                    break;
                default :
                    $device_type_to_number = 0;
            }
            $jsonData = array(
                "app_id" => $this->app_id,
                "identifier" => $device_token,
                "language" => "en",
                "device_type" => $device_type_to_number,
                "test_type" => 1
            );

            $result = $this->guzzle_http->request('POST','https://onesignal.com/api/v1/players', [
                'verify' => false,
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'json' => $jsonData
            ]);
            return $result->getBody()->getContents();
        }catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @desc send push notification
     *
     * @param array $data
     * @param array $message
     * @param array $option
     *
     * @return mixed
     * */
    public function sendPushNotification($data = array(), $message = array(), $option = array())
    {
        try{
            $body = array(
                "app_id" => $this->app_id,
                "headings" => array(
                    "en" => @$message['headings']
                ),
                "data" => array(),
                "contents" => array(
                    "en" => @$message['contents']
                )
            );

            if (isset($data['include_player_ids'])) {
                if (is_array($data['include_player_ids'])) {
                    $include_player_ids = $data['include_player_ids'];
                    $body['include_player_ids'] = $include_player_ids;
                }
            }

            if (isset($data['include_ios_tokens'])) {
                if (is_array($data['include_ios_tokens'])) {
                    $include_ios_tokens = $data['include_ios_tokens'];
                    $body['include_ios_tokens'] = $include_ios_tokens;
                }
            }

            if (isset($data['include_android_reg_ids'])) {
                if (is_array($data['include_android_reg_ids'])) {
                    $include_android_reg_ids = $data['include_android_reg_ids'];
                    $body['include_android_reg_ids'] = $include_android_reg_ids;
                }
            }

            if (is_array($option)) {
                $body = array_merge($body, $option);
            }
            $result = $this->guzzle_http->request('POST', $this->api_url_notification, [
                'verify' => false,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => $this->api_api_key
                ],
                'json' => $body
            ]);

            return $result->getBody()->getContents();
        }catch (\Exception $e) {
            return $e->getMessage();
        }

    }


}