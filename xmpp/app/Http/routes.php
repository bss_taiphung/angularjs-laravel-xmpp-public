<?php

Route::group([
    'namespace' => 'Api',
    'prefix' => 'api'
], function () {
    Route::group([
        'prefix' => 'xmpp',
        'namespace' => 'XMPP'
    ], function () {

        Route::group([
            'prefix' => 'auth',
        ], function () {
            Route::post('register', ['uses'=>'AuthController@register','as'=>'api.xmpp.auth.register']);
            Route::post('change_password', ['uses'=>'AuthController@change_password','as'=>'api.xmpp.auth.change_password']);
            Route::post('check_password', ['uses'=>'AuthController@check_password','as'=>'api.xmpp.auth.check_password']);
            Route::post('check_password_hash', ['uses'=>'AuthController@check_password_hash','as'=>'api.xmpp.auth.check_password_hash']);
        });

        Route::group([
            'prefix' => 'user',
        ], function () {
            Route::post('unregister', ['uses'=>'UserController@unregister','as'=>'api.xmpp.user.unregister']);
            Route::post('registered_users', ['uses'=>'UserController@registered_users','as'=>'api.xmpp.user.registered_users']);
            Route::post('set_vcard2', ['uses'=>'UserController@set_vcard2','as'=>'api.xmpp.user.set_vcard2']);
            Route::post('set_vcard2_multi', ['uses'=>'UserController@set_vcard2_multi','as'=>'api.xmpp.user.set_vcard2_multi']);
            Route::post('get_vcard', ['uses'=>'UserController@get_vcard','as'=>'api.xmpp.user.get_vcard']);
            Route::post('get_vcard2', ['uses'=>'UserController@get_vcard2','as'=>'api.xmpp.user.get_vcard2']);


            Route::post('ban_account', ['uses'=>'UserController@ban_account','as'=>'api.xmpp.user.ban_account']);
            Route::post('get_last', ['uses'=>'UserController@get_last','as'=>'api.xmpp.user.get_last']);
            Route::post('add_rosteritem', ['uses'=>'UserController@add_rosteritem','as'=>'api.xmpp.user.add_rosteritem']);
            Route::post('delete_old_users', ['uses'=>'UserController@delete_old_users','as'=>'api.xmpp.user.delete_old_users']);
            Route::post('delete_old_users_vhost', ['uses'=>'UserController@delete_old_users_vhost','as'=>'api.xmpp.user.delete_old_users_vhost']);
            Route::post('delete_rosteritem', ['uses'=>'UserController@delete_rosteritem','as'=>'api.xmpp.user.delete_rosteritem']);
            Route::post('process_rosteritems', ['uses'=>'UserController@process_rosteritems','as'=>'api.xmpp.user.process_rosteritems']);
            Route::post('user_sessions_info', ['uses'=>'UserController@user_sessions_info','as'=>'api.xmpp.user.user_sessions_info']);

            Route::post('send_message', ['uses'=>'UserController@send_message','as'=>'api.xmpp.user.send_message']);
            Route::post('num_active_users', ['uses'=>'UserController@num_active_users','as'=>'api.xmpp.user.num_active_users']);
            Route::post('num_resources', ['uses'=>'UserController@num_resources','as'=>'api.xmpp.user.num_resources']);
            Route::post('outgoing_s2s_number', ['uses'=>'UserController@outgoing_s2s_number','as'=>'api.xmpp.user.outgoing_s2s_number']);
            Route::post('privacy_set', ['uses'=>'UserController@privacy_set','as'=>'api.xmpp.user.privacy_set']);
            Route::post('private_get', ['uses'=>'UserController@private_get','as'=>'api.xmpp.user.private_get']);
            Route::post('push_alltoall', ['uses'=>'UserController@push_alltoall','as'=>'api.xmpp.user.push_alltoall']);
            Route::post('push_roster', ['uses'=>'UserController@push_roster','as'=>'api.xmpp.user.push_roster']);
            Route::post('push_roster_all', ['uses'=>'UserController@push_roster_all','as'=>'api.xmpp.user.push_roster_all']);
            Route::post('resource_num', ['uses'=>'UserController@resource_num','as'=>'api.xmpp.user.resource_num']);
            Route::post('registered_vhosts', ['uses'=>'UserController@registered_vhosts','as'=>'api.xmpp.user.registered_vhosts']);
            Route::post('send_stanza', ['uses'=>'UserController@send_stanza','as'=>'api.xmpp.user.send_stanza']);
            Route::post('send_stanza_c2s', ['uses'=>'UserController@send_stanza_c2s','as'=>'api.xmpp.user.send_stanza_c2s']);
            Route::post('set_last', ['uses'=>'UserController@set_last','as'=>'api.xmpp.user.set_last']);
            Route::post('set_nickname', ['uses'=>'UserController@set_nickname','as'=>'api.xmpp.user.set_nickname']);
            Route::post('set_presence', ['uses'=>'UserController@set_presence','as'=>'api.xmpp.user.set_presence']);
            Route::post('splitjid', ['uses'=>'UserController@splitjid','as'=>'api.xmpp.user.splitjid']);
            Route::post('splitjids', ['uses'=>'UserController@splitjids','as'=>'api.xmpp.user.splitjids']);
            Route::post('stats', ['uses'=>'UserController@stats','as'=>'api.xmpp.user.stats']);
            Route::post('stats_host', ['uses'=>'UserController@stats_host','as'=>'api.xmpp.user.stats_host']);
            Route::post('status_list', ['uses'=>'UserController@status_list','as'=>'api.xmpp.user.status_list']);
            Route::post('status_num', ['uses'=>'UserController@status_num','as'=>'api.xmpp.user.status_num']);
            Route::post('status_num_host', ['uses'=>'UserController@status_num_host','as'=>'api.xmpp.user.status_num_host']);
            Route::post('user_resources', ['uses'=>'UserController@user_resources','as'=>'api.xmpp.user.user_resources']);
        });

        Route::group([
            'prefix' => 'group',
        ], function () {
            Route::post('muc_online_rooms', ['uses'=>'GroupController@muc_online_rooms','as'=>'api.xmpp.group.muc_online_rooms']);
            Route::post('get_user_rooms', ['uses'=>'GroupController@get_user_rooms','as'=>'api.xmpp.group.get_user_rooms']);
            Route::post('muc_unregister_nick', ['uses'=>'GroupController@muc_unregister_nick','as'=>'api.xmpp.group.muc_unregister_nick']);
            Route::post('kick_user', ['uses'=>'GroupController@kick_user','as'=>'api.xmpp.group.kick_user']);
            Route::post('rooms_unused_list', ['uses'=>'GroupController@rooms_unused_list','as'=>'api.xmpp.group.rooms_unused_list']);
            Route::post('send_direct_invitation', ['uses'=>'GroupController@send_direct_invitation','as'=>'api.xmpp.group.send_direct_invitation']);
            Route::post('set_room_affiliation', ['uses'=>'GroupController@set_room_affiliation','as'=>'api.xmpp.group.set_room_affiliation']);
            Route::post('srg_create', ['uses'=>'GroupController@srg_create','as'=>'api.xmpp.group.srg_create']);
            Route::post('srg_delete', ['uses'=>'GroupController@srg_delete','as'=>'api.xmpp.group.srg_delete']);
            Route::post('srg_get_info', ['uses'=>'GroupController@srg_get_info','as'=>'api.xmpp.group.srg_get_info']);
            Route::post('srg_get_members', ['uses'=>'GroupController@srg_get_members','as'=>'api.xmpp.group.srg_get_members']);
            Route::post('srg_list', ['uses'=>'GroupController@srg_list','as'=>'api.xmpp.group.srg_list']);
            Route::post('srg_user_add', ['uses'=>'GroupController@srg_user_add','as'=>'api.xmpp.group.srg_user_add']);
            Route::post('srg_user_del', ['uses'=>'GroupController@srg_user_del','as'=>'api.xmpp.group.srg_user_del']);


        });

    });


});