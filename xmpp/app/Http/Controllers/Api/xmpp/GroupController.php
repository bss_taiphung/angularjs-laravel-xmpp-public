<?php

namespace App\Http\Controllers\Api\XMPP;

use Illuminate\Http\Request;
use Swagger\Annotations as SWG;
use App\Http\Controllers\ApiBaseController;
use GuzzleHttp\Psr7;
use GuzzleHttp;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   resourcePath="/api/xmpp/group",
 *   description="Auth api",
 *   produces="['application/json']"
 * )
 *
 */
class GroupController extends ApiBaseController
{
    public $auth;
    private $guzzle_http;
    private $request_header;

    public function __construct(
        Request $request
    )
    {
        $this->guzzle_http = new GuzzleHttp\Client(
            [
                'base_uri' => env('EJABBERD_RESTFUL')
            ]
        );
        $this->request_header = [
            'Content-Type' => 'application/json; charset=utf-8',
            'Authorization' => 'bearer w070VGNl58xMmlqkauG8MlhzXsJSz2LK'
        ];
        parent::__construct($request);
    }

    /**
     * @SWG\Model(
     *    id="muc_online_rooms",
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/group/muc_online_rooms",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="List existing rooms (‘global’ to get all vhosts)",
     *      nickname="muc_online_rooms",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="muc_online_rooms", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function muc_online_rooms()
    {
        try {
            $data = $this->request->only(array('host'));

            $results = $this->guzzle_http->request('POST', 'muc_online_rooms', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }


    /**
     * @SWG\Model(
     *    id="muc_unregister_nick",
     *  @SWG\Property(name="nick", type="string", required=true, defaultValue="bss1")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/group/muc_unregister_nick",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="List existing rooms (‘global’ to get all vhosts)",
     *      nickname="muc_unregister_nick",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="muc_unregister_nick", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function muc_unregister_nick()
    {
        try {
            $data = $this->request->only(array('host'));

            $results = $this->guzzle_http->request('POST', 'muc_unregister_nick', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }




    /**
     * @SWG\Model(
     *    id="get_user_rooms",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost")
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/xmpp/group/get_user_rooms",
     *   @SWG\Operation(
     *      method="GET",
     *      summary="Get the list of rooms where this user is occupant",
     *      nickname="get_user_rooms",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="get_user_rooms", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function get_user_rooms()
    {
        try {
            $data = $this->request->only(array('user','host'));

            $results = $this->guzzle_http->request('POST', 'get_user_rooms', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="kick_user",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/group/kick_user",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Disconnect user's active sessions",
     *      nickname="kick_user",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="kick_user", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function kick_user()
    {
        try {
            $data = $this->request->only(array('user','host'));

            $results = $this->guzzle_http->request('POST', 'kick_user', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="rooms_unused_list",
     *  @SWG\Property(name="days", type="string", required=true, defaultValue="1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/group/rooms_unused_list",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Disconnect user's active sessions",
     *      nickname="rooms_unused_list",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="rooms_unused_list", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function rooms_unused_list()
    {
        try {
            $data = $this->request->only(array('days','host'));

            $results = $this->guzzle_http->request('POST', 'rooms_unused_list', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }


    /**
     * @SWG\Model(
     *    id="send_direct_invitation",
     *  @SWG\Property(name="name", type="string", required=true, defaultValue="inviation"),
     *  @SWG\Property(name="service", type="string", required=true, defaultValue="service"),
     *  @SWG\Property(name="password", type="string", required=true, defaultValue="password"),
     *  @SWG\Property(name="reason", type="string", required=true, defaultValue="reason"),
     *  @SWG\Property(name="users", type="string", required=true, defaultValue="bss1")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/group/send_direct_invitation",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Send a direct invitation to several destinations",
     *      nickname="send_direct_invitation",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="send_direct_invitation", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function send_direct_invitation()
    {
        try {
            $data = $this->request->only(array('name','service','password','reason','users'));

            $results = $this->guzzle_http->request('POST', 'send_direct_invitation', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="set_room_affiliation",
     *  @SWG\Property(name="name", type="string", required=true, defaultValue="name"),
     *  @SWG\Property(name="service", type="string", required=true, defaultValue="service"),
     *  @SWG\Property(name="jid", type="string", required=true, defaultValue="bss1@localhost"),
     *  @SWG\Property(name="affiliation", type="string", required=true, defaultValue="affiliation")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/group/set_room_affiliation",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Change an affiliation in a MUC room",
     *      nickname="set_room_affiliation",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="set_room_affiliation", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function set_room_affiliation()
    {
        try {
            $data = $this->request->only(array('name','service','jid','affiliation'));

            $results = $this->guzzle_http->request('POST', 'set_room_affiliation', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="srg_create",
     *  @SWG\Property(name="group", type="string", required=true, defaultValue="group"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="name", type="string", required=true, defaultValue="name"),
     *  @SWG\Property(name="description", type="string", required=true, defaultValue="description"),
     *  @SWG\Property(name="display", type="string", required=true, defaultValue="display")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/group/srg_create",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Create a Shared Roster Group",
     *      nickname="srg_create",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="srg_create", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function srg_create()
    {
        try {
            $data = $this->request->only(array('group','host','name','description','display'));

            $results = $this->guzzle_http->request('POST', 'srg_create', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="srg_delete",
     *  @SWG\Property(name="group", type="string", required=true, defaultValue="group"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/group/srg_delete",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Delete a Shared Roster Group",
     *      nickname="srg_delete",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="srg_delete", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function srg_delete()
    {
        try {
            $data = $this->request->only(array('group','host'));

            $results = $this->guzzle_http->request('POST', 'srg_delete', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="srg_get_info",
     *  @SWG\Property(name="group", type="string", required=true, defaultValue="group"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/group/srg_get_info",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Get info of a Shared Roster Group",
     *      nickname="srg_get_info",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="srg_get_info", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function srg_get_info()
    {
        try {
            $data = $this->request->only(array('group','host'));

            $results = $this->guzzle_http->request('POST', 'srg_get_info', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="srg_get_members",
     *  @SWG\Property(name="group", type="string", required=true, defaultValue="group"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/group/srg_get_members",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Get members of a Shared Roster Group",
     *      nickname="srg_get_members",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="srg_get_members", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function srg_get_members()
    {
        try {
            $data = $this->request->only(array('group','host'));

            $results = $this->guzzle_http->request('POST', 'srg_get_members', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="srg_list",
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/group/srg_list",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="List the Shared Roster Groups in Host",
     *      nickname="srg_list",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="srg_list", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function srg_list()
    {
        try {
            $data = $this->request->only(array('host'));

            $results = $this->guzzle_http->request('POST', 'srg_list', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="srg_user_add",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="group", type="string", required=true, defaultValue="group"),
     *  @SWG\Property(name="grouphost", type="string", required=true, defaultValue="localhost")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/group/srg_user_add",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Add the JID user@host to the Shared Roster Group",
     *      nickname="srg_user_add",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="srg_user_add", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function srg_user_add()
    {
        try {
            $data = $this->request->only(array('user','host','group','grouphost'));

            $results = $this->guzzle_http->request('POST', 'srg_user_add', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }


    /**
     * @SWG\Model(
     *    id="srg_user_del",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="group", type="string", required=true, defaultValue="group"),
     *  @SWG\Property(name="grouphost", type="string", required=true, defaultValue="localhost")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/group/srg_user_del",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Delete this JID user@host from the Shared Roster Group",
     *      nickname="srg_user_del",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="srg_user_del", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function srg_user_del()
    {
        try {
            $data = $this->request->only(array('user','host','group','grouphost'));

            $results = $this->guzzle_http->request('POST', 'srg_user_del', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }
}