<?php

namespace App\Http\Controllers\Api\XMPP;

use Illuminate\Http\Request;
use Swagger\Annotations as SWG;
use App\Http\Controllers\ApiBaseController;
use GuzzleHttp\Psr7;
use GuzzleHttp;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   resourcePath="/api/xmpp/auth",
 *   description="Auth api",
 *   produces="['application/json']"
 * )
 *
 */
class AuthController extends ApiBaseController
{
    public $auth;
    private $guzzle_http;
    private $request_header;

    public function __construct(
        Request $request
    )
    {
        $this->guzzle_http = new GuzzleHttp\Client(
            [
                'base_uri' => env('EJABBERD_RESTFUL')
            ]
        );
        $this->request_header = [
            'Content-Type' => 'application/json; charset=utf-8',
            'Authorization' => 'bearer q8h1o4iftsJ0BZCXwh1uaOx4z1upRsuv',
            'X-Admin' => true
        ];
        parent::__construct($request);
    }
    /**
     * @SWG\Model(
     *    id="register",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss3"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="password", type="string", required=true, defaultValue="password"),
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/xmpp/auth/register",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Register User",
     *      nickname="Register User",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="register", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function register()
    {
        try {
            $data = $this->request->only(array('user', 'host', 'password'));
            $results = $this->guzzle_http->request('POST', 'register', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }
    /**
     * @SWG\Model(
     *    id="change_password",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="newpass", type="string", required=true, defaultValue="123456"),
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/xmpp/auth/change_password",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Change Password",
     *      nickname="ChangePassword",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="change_password", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function change_password()
    {

        try {
            $data = $this->request->only(array('user', 'host', 'newpass'));
            $results = $this->guzzle_http->request('POST', 'change_password', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="check_password",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="password", type="string", required=true, defaultValue="password"),
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/auth/check_password",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Check if user password is correct",
     *      nickname="CheckPassword",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="check_password", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function check_password()
    {
        try {
            $data = $this->request->only(array('user', 'host', 'password'));
            $results = $this->guzzle_http->request('POST', 'check_password', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="check_password_hash",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="passwordhash", type="string", required=true, defaultValue="123456789"),
     *  @SWG\Property(name="hashmethod", type="string", required=true, defaultValue="md5"),
     *
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/auth/check_password_hash",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Check if user hash password is correct",
     *      nickname="CheckHashPassword",
     *      @SWG\Parameter(name="body", description="Allowed hash method: md5, sha", required=true, type="check_password_hash", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function check_password_hash()
    {
        try {
            $data = $this->request->only(array('user', 'host', 'passwordhash','hashmethod'));
            $results = $this->guzzle_http->request('POST', 'check_password_hash', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }
}