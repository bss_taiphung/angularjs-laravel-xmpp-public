<?php

namespace App\Http\Controllers\Api\XMPP;

use Illuminate\Http\Request;
use Swagger\Annotations as SWG;
use App\Http\Controllers\ApiBaseController;
use GuzzleHttp\Psr7;
use GuzzleHttp;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   resourcePath="/api/xmpp/user",
 *   description="Auth api",
 *   produces="['application/json']"
 * )
 *
 */
class UserController extends ApiBaseController
{
    public $auth;
    private $guzzle_http;
    private $request_header;

    public function __construct(
        Request $request
    )
    {
        $this->guzzle_http = new GuzzleHttp\Client(
            [
                'base_uri' => env('EJABBERD_RESTFUL')
            ]
        );
        $this->request_header = [
            'Content-Type' => 'application/json; charset=utf-8',
            'Authorization' => 'bearer w070VGNl58xMmlqkauG8MlhzXsJSz2LK'
        ];
        parent::__construct($request);
    }
    /**
     * @SWG\Model(
     *    id="registered_users",
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/registered_users",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="List all registered users in HOST",
     *      nickname="registered_users",
     *      @SWG\Parameter(name="host", description="Request body", required=true, type="registered_users", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function registered_users()
    {
        try {
            $data = $this->request->only('host');

            $results = $this->guzzle_http->request('POST', 'registered_users', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="registered_vhosts"
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/registered_vhosts",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="List all registered vhosts in SERVER",
     *      nickname="registered_vhosts",
     *      @SWG\Parameter(name="host", description="Request body", required=true, type="registered_vhosts", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function registered_vhosts()
    {
        try {
            $data = $this->request->only('host');

            $results = $this->guzzle_http->request('POST', 'registered_vhosts', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }


    /**
     * @SWG\Model(
     *    id="set_vcard",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="name", type="string", required=true, defaultValue="BSS1"),
     *  @SWG\Property(name="content", type="string", required=true, defaultValue="")
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/set_vcard",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Set content in a vCard field",
     *      nickname="set_vcard",
     *      @SWG\Parameter(name="host", description="Request body", required=true, type="set_vcard", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function set_vcard()
    {
        try {
            $data = $this->request->only(array(
                'user',
                'host',
                'name',
                'content'
            ));

            $results = $this->guzzle_http->request('POST', 'set_vcard', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="set_vcard2",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="name", type="string", required=true, defaultValue="BSS1"),
     *  @SWG\Property(name="subname", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="content", type="string", required=true, defaultValue="")
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/set_vcard2",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Set content in a vCard subfield",
     *      nickname="set_vcard2",
     *      @SWG\Parameter(name="host", description="Request body", required=true, type="set_vcard2", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function set_vcard2()
    {
        try {
            $data = $this->request->only(array(
                'user',
                'host',
                'name',
                'subname',
                'content'
            ));

            $results = $this->guzzle_http->request('POST', 'set_vcard2', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }


    /**
     * @SWG\Model(
     *    id="set_vcard2_multi",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="name", type="string", required=true, defaultValue="BSS1"),
     *  @SWG\Property(name="subname", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="contents", type="string", required=true, defaultValue="[]")
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/set_vcard2_multi",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Set multiple contents in a vCard subfield",
     *      nickname="set_vcard2_multi",
     *      @SWG\Parameter(name="host", description="Request body", required=true, type="set_vcard2_multi", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function set_vcard2_multi()
    {
        try {
            $data = $this->request->only(array(
                'user',
                'host',
                'name',
                'subname',
                'contents'
            ));

            $results = $this->guzzle_http->request('POST', 'set_vcard2_multi', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="get_vcard",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="name", type="string", required=true, defaultValue="bss1"),
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/get_vcard",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Get content from a vCard field",
     *      nickname="get_vcard",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="get_vcard", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function get_vcard()
    {
        try {

            $data = $this->request->only(array(
                'user',
                'host',
                'name'
            ));

            $results = $this->guzzle_http->request('POST', 'get_vcard', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());

        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }


    /**
     * @SWG\Model(
     *    id="get_vcard2",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="name", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="subname", type="string", required=true, defaultValue="bss1"),
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/get_vcard2",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Get content from a vCard field",
     *      nickname="get_vcard2",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="get_vcard2", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function get_vcard2()
    {
        try {
            $data = $this->request->only(array(
                'user',
                'host',
                'name',
                'subname'
            ));
            $results = $this->guzzle_http->request('POST', 'get_vcard2', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());

        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }


    /**
     * @SWG\Model(
     *    id="unregister",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/unregister",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Unregister a user",
     *      nickname="unregister",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="unregister", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function unregister()
    {
        try {
            $data = $this->request->only(array('host','user'));

            $results = $this->guzzle_http->request('POST', 'unregister', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }


    /**
     * @SWG\Model(
     *    id="ban_account",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="reason", type="string", required=true, defaultValue="Bad"),
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/ban_account",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Ban an account: kick sessions and set random password",
     *      nickname="ban_account",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="ban_account", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function ban_account()
    {
        try {
            $data = $this->request->only(array('host','user'));

            $results = $this->guzzle_http->request('POST', 'ban_account', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="get_last",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost")
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/get_last",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Get last activity information (timestamp and status)",
     *      nickname="get_last",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="get_last", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function get_last()
    {
        try {
            $data = $this->request->only(array('host','user'));

            $results = $this->guzzle_http->request('POST', 'get_last', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }
    /**
     * @SWG\Model(
     *    id="user_sessions_info",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user_sessions_info",
     *   @SWG\Operation(
     *      method="POST",
     *      summary=" Get information about all sessions of a user",
     *      nickname="KickUser",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="user_sessions_info", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function user_sessions_info()
    {
        try {
            $data = $this->request->only(array('host','user'));

            $results = $this->guzzle_http->request('POST', 'get_last', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="send_message",
     *  @SWG\Property(name="type", type="string", required=true, defaultValue="normal"),
     *  @SWG\Property(name="from", type="string", required=true, defaultValue="bss1@localhost"),
     *  @SWG\Property(name="to", type="string", required=true, defaultValue="bss2@localhost"),
     *  @SWG\Property(name="subject", type="string", required=true, defaultValue="subject"),
     *  @SWG\Property(name="content", type="string", required=true, defaultValue="content"),
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/send_message",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Send a message to a local or remote bare of full JID",
     *      nickname="send_message",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="send_message", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function send_message()
    {
        try {
            $data = $this->request->only(array('type','from','to','subject','content'));

            $results = $this->guzzle_http->request('POST', 'send_message', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="add_rosteritem",
     *  @SWG\Property(name="localuser", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="localserver", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss2"),
     *  @SWG\Property(name="server", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="nick", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="group", type="string", required=true, defaultValue="group")
     *
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/add_rosteritem",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Add an item to a user’s roster (supports SQL",
     *      nickname="add_rosteritem",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="add_rosteritem", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function add_rosteritem()
    {
        try {
            $data = $this->request->only(array(
                'localuser',
                'localserver',
                'user',
                'server',
                'nick',
                'group'
            ));

            $results = $this->guzzle_http->request('POST', 'add_rosteritem', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="delete_old_users",
     *  @SWG\Property(name="days", type="string", required=true, defaultValue="1")
     *
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/delete_old_users",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Delete users that didn't log in last days, or that never logged",
     *      nickname="delete_old_users",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="delete_old_users", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function delete_old_users()
    {
        try {
            $data = $this->request->only(array('days'));

            $results = $this->guzzle_http->request('POST', 'add_rosteritem', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="delete_old_users_vhost",
     *  @SWG\Property(name="days", type="string", required=true, defaultValue="1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/delete_old_users_vhost",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Delete users that didn't log in last days in vhost, or that never logged",
     *      nickname="delete_old_users_vhost",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="delete_old_users_vhost", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function delete_old_users_vhost()
    {
        try {
            $data = $this->request->only(array('days','host'));

            $results = $this->guzzle_http->request('POST', 'delete_old_users_vhost', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="delete_rosteritem",
     *  @SWG\Property(name="localuser", type="string", required=true, defaultValue="user1"),
     *  @SWG\Property(name="localserver", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="user2"),
     *  @SWG\Property(name="server", type="string", required=true, defaultValue="localhost"),
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/delete_rosteritem",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Delete an item from a user’s roster (supports SQL)",
     *      nickname="delete_rosteritem",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="delete_rosteritem", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function delete_rosteritem()
    {
        try {
            $data = $this->request->only(array(
                'localuser',
                'localserver',
                'user',
                'server'
            ));

            $results = $this->guzzle_http->request('POST', 'delete_rosteritem', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }
    /**
     * @SWG\Model(
     *    id="process_rosteritems",
     *  @SWG\Property(name="action", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="subs", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="asks", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="users", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="contacts", type="string", required=true, defaultValue="bss1"),
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/process_rosteritems",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="List or delete rosteritems that match filtering options",
     *      nickname="GetProfile",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="process_rosteritems", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function process_rosteritems()
    {
        try {
            $data = $this->request->only(array(
                'action',
                'subs',
                'asks',
                'users',
                'contacts'
            ));

            $results = $this->guzzle_http->request('POST', 'process_rosteritems', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="num_active_users",
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="days", type="string", required=true, defaultValue="1")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/num_active_users",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Get number of users active in the last days",
     *      nickname="num_active_users",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="num_active_users", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function num_active_users()
    {
        try {
            $data = $this->request->only(array( 'host','days'));

            $results = $this->guzzle_http->request('POST', 'num_active_users', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="num_resources",
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/num_resources",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Get the number of resources of a users",
     *      nickname="num_resources",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="num_resources", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function num_resources()
    {
        try {
            $data = $this->request->only(array('host','user'));

            $results = $this->guzzle_http->request('POST', 'num_resources', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="outgoing_s2s_number"
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/outgoing_s2s_number",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Number of outgoing s2s connections on the node",
     *      nickname="outgoing_s2s_number",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="outgoing_s2s_number", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function outgoing_s2s_number()
    {
        try {
            $data = array();

            $results = $this->guzzle_http->request('POST', 'outgoing_s2s_number', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="privacy_set",
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="xmlquery", type="string", required=true, defaultValue="query")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/privacy_set",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Send a IQ set privacy stanza for a local account",
     *      nickname="privacy_set",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="privacy_set", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function privacy_set()
    {
        try {
            $data = $this->request->only(array('host','user','xmlquery'));

            $results = $this->guzzle_http->request('POST', 'privacy_set', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="private_get",
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="element", type="string", required=true, defaultValue="element"),
     *  @SWG\Property(name="ns", type="string", required=true, defaultValue="ns")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/private_get",
     *   @SWG\Operation(
     *      method="POST",
     *      summary=" Get some information from a user private storage",
     *      nickname="private_get",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="private_get", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function private_get()
    {
        try {
            $data = $this->request->only(array('host','user','element','ns'));

            $results = $this->guzzle_http->request('POST', 'private_get', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="push_alltoall",
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="group", type="string", required=true, defaultValue="group")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/push_alltoall",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Add all the users to all the users of Host in Group",
     *      nickname="push_alltoall",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="push_alltoall", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function push_alltoall()
    {
        try {
            $data = $this->request->only(array('host','group'));

            $results = $this->guzzle_http->request('POST', 'push_alltoall', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="push_roster",
     *  @SWG\Property(name="file", type="string", required=true, defaultValue="file"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/push_roster",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Push template roster from file to a user",
     *      nickname="push_roster",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="push_roster", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function push_roster()
    {
        try {
            $data = $this->request->only(array('file','host','user'));

            $results = $this->guzzle_http->request('POST', 'push_roster', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="push_roster_all",
     *  @SWG\Property(name="file", type="string", required=true, defaultValue="file")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/push_roster_all",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Push template roster from file to all those users",
     *      nickname="push_roster_all",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="push_roster_all", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function push_roster_all()
    {
        try {
            $data = $this->request->only(array('file'));

            $results = $this->guzzle_http->request('POST', 'push_roster_all', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="resource_num",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="num", type="string", required=true, defaultValue="1")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/resource_num",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Push template roster from file to all those users",
     *      nickname="resource_num",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="resource_num", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function resource_num()
    {
        try {
            $data = $this->request->only(array('user','host','num'));

            $results = $this->guzzle_http->request('POST', 'resource_num', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }


    /**
     * @SWG\Model(
     *    id="send_stanza",
     *  @SWG\Property(name="from", type="string", required=true, defaultValue="bss1@localhost"),
     *  @SWG\Property(name="to", type="string", required=true, defaultValue="bss2@localhost"),
     *  @SWG\Property(name="stanza", type="string", required=true, defaultValue="stanza")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/send_stanza",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Send a stanza; provide From JID and valid To JID",
     *      nickname="send_stanza",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="send_stanza", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function send_stanza()
    {
        try {
            $data = $this->request->only(array('from','to','stanza'));

            $results = $this->guzzle_http->request('POST', 'send_stanza', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="send_stanza_c2s",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="resource", type="string", required=true, defaultValue="resource"),
     *  @SWG\Property(name="stanza", type="string", required=true, defaultValue="stanza")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/send_stanza_c2s",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Send a stanza as if sent from a c2s session",
     *      nickname="send_stanza_c2s",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="send_stanza_c2s", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function send_stanza_c2s()
    {
        try {
            $data = $this->request->only(array('user','host','resource','stanza'));

            $results = $this->guzzle_http->request('POST', 'send_stanza_c2s', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="set_last",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="timestamp", type="string", required=true, defaultValue="111111"),
     *  @SWG\Property(name="status", type="string", required=true, defaultValue="status")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/set_last",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Set last activity information",
     *      nickname="set_last",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="set_last", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function set_last()
    {
        try {
            $data = $this->request->only(array('user','host','timestamp','status'));

            $results = $this->guzzle_http->request('POST', 'set_last', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="set_nickname",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="nickname", type="string", required=true, defaultValue="nickname")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/set_nickname",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Set nickname in a user’s vCard",
     *      nickname="set_nickname",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="set_nickname", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function set_nickname()
    {
        try {
            $data = $this->request->only(array('user','host','nickname'));

            $results = $this->guzzle_http->request('POST', 'set_nickname', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="set_presence",
     *  @SWG\Property(name="user", type="string", required=true, defaultValue="bss1"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="resource", type="string", required=true, defaultValue="localhost"),
     *  @SWG\Property(name="type", type="string", required=true, defaultValue="nickname"),
     *  @SWG\Property(name="show", type="string", required=true, defaultValue="nickname"),
     *  @SWG\Property(name="status", type="string", required=true, defaultValue="nickname"),
     *  @SWG\Property(name="priority", type="string", required=true, defaultValue="nickname")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/set_presence",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Set presence of a session",
     *      nickname="set_presence",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="set_presence", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function set_presence()
    {
        try {
            $data = $this->request->only(array('user','host','resource','type','show','status','priority'));

            $results = $this->guzzle_http->request('POST', 'set_presence', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="splitjid",
     *  @SWG\Property(name="jid", type="string", required=true, defaultValue="bss1@localhost")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/splitjid",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Split JID in parts: user, server, resource",
     *      nickname="splitjid",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="splitjid", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function splitjid()
    {
        try {
            $data = $this->request->only(array('jid'));

            $results = $this->guzzle_http->request('POST', 'splitjid', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="splitjids",
     *  @SWG\Property(name="jids", type="string", required=true, defaultValue="[bss1@localhost,bss2@localhost]")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/splitjids",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Split JIDs in parts: user, server, resource",
     *      nickname="splitjids",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="splitjids", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function splitjids()
    {
        try {
            $data = $this->request->only(array('jids'));

            $results = $this->guzzle_http->request('POST', 'splitjids', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="stats",
     *  @SWG\Property(name="name", type="string", required=true, defaultValue="name")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/stats",
     *   @SWG\Operation(
     *      method="POST",
     *      summary=" Get statistical value: registeredusers onlineusers onlineusersnode uptimeseconds processes",
     *      nickname="stats",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="stats", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function stats()
    {
        try {
            $data = $this->request->only(array('name'));

            $results = $this->guzzle_http->request('POST', 'stats', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="stats_host",
     *  @SWG\Property(name="name", type="string", required=true, defaultValue="name"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/stats_host",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Get statistical value for this host: registeredusers onlineusers",
     *      nickname="stats_host",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="stats_host", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function stats_host()
    {
        try {
            $data = $this->request->only(array('name'));

            $results = $this->guzzle_http->request('POST', 'stats_host', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="status_list",
     *  @SWG\Property(name="status", type="string", required=true, defaultValue="available")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/status_list",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="List of logged users with this status",
     *      nickname="status_list",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="status_list", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function status_list()
    {
        try {
            $data = $this->request->only(array('status'));

            $results = $this->guzzle_http->request('POST', 'status_list', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="status_list_host",
     *  @SWG\Property(name="status", type="string", required=true, defaultValue="available"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/status_list_host",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="List of users logged in host with their statuses",
     *      nickname="status_list_host",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="status_list_host", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function status_list_host()
    {
        try {
            $data = $this->request->only(array('status','host'));

            $results = $this->guzzle_http->request('POST', 'status_list_host', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="status_num",
     *  @SWG\Property(name="status", type="string", required=true, defaultValue="available")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/status_num",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Number of logged users with this status",
     *      nickname="status_num",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="status_num", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function status_num()
    {
        try {
            $data = $this->request->only(array('status'));

            $results = $this->guzzle_http->request('POST', 'status_num', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="status_num_host",
     *  @SWG\Property(name="status", type="string", required=true, defaultValue="available"),
     *  @SWG\Property(name="host", type="string", required=true, defaultValue="localhost")
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/status_num_host",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Number of logged users with this status in host",
     *      nickname="status_num_host",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="status_num_host", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function status_num_host()
    {
        try {
            $data = $this->request->only(array('host','status'));

            $results = $this->guzzle_http->request('POST', 'status_num_host', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

    /**
     * @SWG\Model(
     *    id="user_resources"
     * )
     */
    /**
     * @SWG\Api(
     *   path="/api/xmpp/user/user_resources",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="List user’s connected resources",
     *      nickname="user_resources",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="user_resources", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function user_resources()
    {
        try {
            $data = array();

            $results = $this->guzzle_http->request('POST', 'user_resources', [
                'verify' => false,
                'headers' => $this->request_header,
                'json' => $data
            ]);
            return $this->respondWithSuccess($results->getBody()->getContents());
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 500);
        }
    }

}