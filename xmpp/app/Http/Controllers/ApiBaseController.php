<?php namespace App\Http\Controllers;


use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Common\ErrorFormat;
use Illuminate\Routing\Controller;
use Illuminate\Contracts\Validation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Helpers\OneSignal;
abstract class ApiBaseController extends Controller
{
    use DispatchesJobs;

    /**
     * HTTP header status code.
     *
     * @var int
     */
    protected $statusCode = 200;

    /**
     * Illuminate\Http\Request instance.
     *
     * @var Request
     */
    protected $request;

    /**
     * @var Validator
     */
    public $validator;

    /**
     * @var $auth
     */
    public $auth;

    /**
     * @var $apiErrorCodes
     */
    public $apiErrorCodes;

    /**
     * @var $apiSuccessMessage
     */
    public $apiSuccessMessage;

    /**
     * @var $apiSuccessMessage
     */
    public $apiErrorMessage;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->apiSuccessMessage = Lang::get('apiSuccessMessage');
        $this->apiErrorMessage = Lang::get('apiErrorMessage');

        if($this->request->get('auth')){
            $this->auth = $this->request->get('auth');
            $this->request->attributes->remove('auth');
        }
    }


    /**
     * Getter for statusCode.
     *
     * @return int
     */
    protected function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Setter for statusCode.
     *
     * @param int $statusCode Value to set
     *
     * @return self
     */
    protected function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @param  string $errorMessage
     * @param  int $errorCode
     * @param  null $statusCode
     * @param  array $headers
     * @return mixed
     */
    protected function respondWithErrorMessage($errorMessage, $errorCode = null, $statusCode = null, array $headers = [])
    {
        // if status code not change to error status => set it 400 error
        if (is_null($statusCode)) {
            $this->setStatusCode(400);
        } else {
            $this->setStatusCode($statusCode);
        }
        $parseErrors = array();
        $parseErrors[] = new ErrorFormat($errorMessage, $errorCode);

        $response = array(
            'error' => true,
            'data' => null,
            'errors' => $parseErrors
        );
        return response()->json($response, $this->statusCode, $headers);
    }


    /**
     * @param  \App\Common\ErrorFormat[] $errors
     * @param  null $statusCode
     * @param  array $headers
     * @return mixed
     */
    protected function respondWithError($errors, $statusCode = null, array $headers = [])
    {
        // if status code not change to error status => set it 400 error
        if (is_null($statusCode)) {
            $this->setStatusCode(400);
        } else {
            $this->setStatusCode($statusCode);
        }
        $parseErrors = array();
        foreach ($errors as $error) {
            $parseErrors[] = new ErrorFormat($error[0], intval($error[1]));
        }

        $response = array(
            'error' => true,
            'data' => null,
            'errors' => $parseErrors
        );
        return response()->json($response, $this->statusCode, $headers);
    }

    /**
     * @param    {array|object|string} $data
     * @param    array $headers
     * @return                         mixed
     */
    protected function respondWithSuccess($data, $statusCode = null, array $headers = [])
    {
        // if status code not change to error status => set it 400 error
        if (is_null($statusCode)) {
            $this->setStatusCode(200);
        } else {
            $this->setStatusCode($statusCode);
        }

        $response = array(
            'error' => false,
            'data' => $data,
            'errors' => null
        );

        return response()->json($response, $this->statusCode, $headers);
    }


    /**
     * Generate a Response with a 403 HTTP header and a given message.
     *
     * @param   string $message
     * @param   int $errorCode
     * @param    array $headers
     * @return  mixed
     */
    protected function errorForbidden($message = 'Forbidden', $errorCode = 0, array $headers = [])
    {
        return $this->respondWithErrorMessage($message, $errorCode, 403, $headers);
    }

    /**
     * Generate a Response with a 500 HTTP header and a given message.
     *
     * @param   string $message
     * @param   int $errorCode
     * @param    array $headers
     *
     * @return Response
     */
    protected function errorInternalError($message = 'Internal Error', $errorCode = 0, array $headers = [])
    {
        return $this->respondWithErrorMessage($message, $errorCode, 500, $headers);
    }

    /**
     * Generate a Response with a 404 HTTP header and a given message.
     *
     * @param   string $message
     * @param   int $errorCode
     * @param    array $headers
     *
     * @return Response
     */
    protected function errorNotFound($message = 'Resource Not Found', $errorCode = 0, array $headers = [])
    {
        return $this->respondWithErrorMessage($message, $errorCode, 404, $headers);
    }

    /**
     * Generate a Response with a 401 HTTP header and a given message.
     *
     * @param   string $message
     * @param   int $errorCode
     * @param    array $headers
     *
     * @return Response
     */
    protected function errorUnauthorized($message = 'Unauthorized', $errorCode = 0, array $headers = [])
    {
        return $this->respondWithErrorMessage($message, $errorCode, 401, $headers);
    }

    /**
     * Generate a Response with a 400 HTTP header and a given message.
     *
     * @param   string $message
     * @param   int $errorCode
     * @param    array $headers
     *
     * @return Response
     */
    protected function errorWrongArgs($message = 'Wrong Arguments', $errorCode = 0, array $headers = [])
    {
        return $this->respondWithErrorMessage($message, $errorCode, 400, $headers);
    }

    /**
     * Generate a Response with a 501 HTTP header and a given message.
     *
     * @param   string $message
     * @param   int $errorCode
     * @param    array $headers
     *
     * @return Response
     */
    protected function errorNotImplemented($message = 'Not implemented', $errorCode = 0, array $headers = [])
    {
        return $this->respondWithErrorMessage($message, $errorCode, 501, $headers);
    }


    /**
     * Generate a Response with a 400 HTTP header and a given message.
     *
     * @param   Validation $validation
     * @param   int $statusCode
     * @param   array $headers
     *
     * @return Response
     */
    protected function errorWithValidation($validation, $statusCode = null, array $headers = [])
    {
        $errors = ValidateResponse::make($validation);
        return $this->respondWithError($errors, $statusCode, $headers);
    }

    /**
     * @param $content
     * @param $userId
     * @return $pushNotification
     */
    public static function sendPushNotification($userId,$content){
        $oneSignal = new OneSignal();
        // Get user that allow notification
        $userTokenDevices = UserTokenDevice::join('users','user_token_devices.user_id','=','users.id')
            ->where('users.id',$userId)
            ->where('users.notification','<>',0)
            ->get();
        if(count($userTokenDevices) == 0) {
            return null;
        }
        // iOS Device
        if($userTokenDevices->device_type == 0){
            $devices['include_ios_tokens'][] = $userTokenDevices->token;
        }else{ // Android device
            $devices['include_android_reg_ids'][] = $userTokenDevices->token;
        }

        $messages = array(
            'headings' => "LUXR Notification",
            'contents' => $content
        );

        $options = ["ios_badgeType" => "SetTo", "ios_badgeCount"=>1];

        $pushNotification = $oneSignal->sendPushNotification($devices, $messages, $options);

        return $pushNotification;
    }
}
