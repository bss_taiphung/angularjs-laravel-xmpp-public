 add_rosteritem localuser localserver user server nick group subs     Add an item to a user's roster (supports ODBC)
   backup file                                                          Store the database to backup file
   ban_account user host reason                                         Ban an account: kick sessions and set random password
   change_password user host newpass                                    Change the password of an account
   change_room_option name service option value                         Change an option in a MUC room
   check_account user host                                              Check if an account exists or not
   check_password user host password                                    Check if a password is correct
   check_password_hash user host passwordhash hashmethod                Check if the password hash is correct
   compile file                                                         Recompile and reload Erlang source code file
   connected_users                                                      List all established sessions
   connected_users_info                                                 List all established sessions and their information
   connected_users_number                                               Get the number of established sessions
   connected_users_vhost host                                           Get the list of established sessions in a vhost
   convert_to_scram host                                                Convert the passwords in 'users' ODBC table to SCRAM
   convert_to_yaml in out                                               Convert the input file from Erlang to YAML format
   create_room name service host                                        Create a MUC room name@service in host
   create_room_with_opts name service host options                      *Create a MUC room name@service in host with given options
   create_rooms_file file                                               Create the rooms indicated in file
   delete_expired_messages                                              Delete expired offline messages from database
   delete_mnesia host                                                   Delete elements in Mnesia database for a given vhost
   delete_old_mam_messages type days                                    Delete MAM messages older than DAYS
   delete_old_messages days                                             Delete offline messages older than DAYS
   delete_old_users days                                                Delete users that didn't log in last days, or that never logged
   delete_old_users_vhost host days                                     Delete users that didn't log in last days in vhost, or that never logged
   delete_rosteritem localuser localserver user server                  Delete an item from a user's roster (supports ODBC)
   destroy_room name service                                            Destroy a MUC room
   destroy_rooms_file file                                              Destroy the rooms indicated in file
   dump file                                                            Dump the database to a text file
   dump_table file table                                                Dump a table to a text file
   export2sql host file                                                 Export virtual host information from Mnesia tables to SQL file
   export_piefxis dir                                                   Export data of all users in the server to PIEFXIS files (XEP-0227)
   export_piefxis_host dir host                                         Export data of users in a host to PIEFXIS files (XEP-0227)
   gen_html_doc_for_commands file regexp examples                       Generates html documentation for ejabberd_commands
   gen_markdown_doc_for_commands file regexp examples                   Generates markdown documentation for ejabberd_commands
   get_cookie                                                           Get the Erlang cookie of this node
   get_last user host                                                   Get last activity information (timestamp and status)
   get_loglevel                                                         Get the current loglevel
   get_offline_count user server                                        Get the number of unread offline messages
   get_presence user server                                             Retrieve the resource with highest priority, and its presence (show and status message) for a given user.
   get_room_affiliations name service                                   Get the list of affiliations of a MUC room
   get_room_occupants name service                                      Get the list of occupants of a MUC room
   get_room_occupants_number name service                               Get the number of occupants of a MUC room
   get_room_options name service                                        Get options from a MUC room
   get_roster user server                                               Get roster of a local user
   get_subscribers name service                                         List subscribers of a MUC conference
   get_user_rooms user host                                             Get the list of rooms where this user is occupant
   get_vcard user host name                                             Get content from a vCard field
   get_vcard2 user host name subname                                    Get content from a vCard field
   get_vcard2_multi user host name subname                              Get multiple contents from a vCard field
   help [--tags [tag] | com?*]                                          Show help (try: ejabberdctl help help)
   import_dir file                                                      Import users data from jabberd14 spool dir
   import_file file                                                     Import user data from jabberd14 spool file
   import_piefxis file                                                  Import users data from a PIEFXIS file (XEP-0227)
   import_prosody dir                                                   Import data from Prosody
   incoming_s2s_number                                                  Number of incoming s2s connections on the node
   install_fallback file                                                Install the database from a fallback file
   join_cluster node                                                    Join this node into the cluster handled by Node
   kick_session user host resource reason                               Kick a user session
   kick_user user host                                                  Disconnect user's active sessions
   leave_cluster node                                                   Remove and shutdown Node from the running cluster
   list_cluster                                                         List nodes that are part of the cluster handled by Node
   load file                                                            Restore the database from a text file
   mnesia [info]                                                        show information of Mnesia system
   mnesia_change_nodename oldnodename newnodename oldbackup newbackup   Change the erlang node name in a backup file
   module_check module                                                 
   module_install module                                               
   module_uninstall module                                             
   module_upgrade module                                               
   modules_available                                                   
   modules_installed                                                   
   modules_update_specs                                                
   muc_online_rooms host                                                List existing rooms ('global' to get all vhosts)
   muc_unregister_nick nick                                             Unregister the nick in the MUC service
   num_active_users host days                                           Get number of users active in the last days
   num_resources user host                                              Get the number of resources of a user
   oauth_issue_token jid ttl scopes                                     Issue an oauth token for the given jid
   oauth_list_scopes                                                    List scopes that can be granted to tokens generated through the command line, together with the commands they
                                                                      allow
   oauth_list_tokens                                                    List oauth tokens, their user and scope, and how many seconds remain until expirity
   oauth_revoke_token token                                             Revoke authorization for a token
   outgoing_s2s_number                                                  Number of outgoing s2s connections on the node
   privacy_set user host xmlquery                                       Send a IQ set privacy stanza for a local account
   private_get user host element ns                                     Get some information from a user private storage
   private_set user host element                                        Set to the user private storage
   process_rosteritems action subs asks users contacts                  List or delete rosteritems that match filtering options
   push_alltoall host group                                             Add all the users to all the users of Host in Group
   push_roster file user host                                           Push template roster from file to a user
   push_roster_all file                                                 Push template roster from file to all those users
   register user host password                                          Register a user
   registered_users host                                                List all registered users in HOST
   registered_vhosts                                                    List all registered vhosts in SERVER
   reload_config                                                        Reload config file in memory (only affects ACL and Access)
   reopen_log                                                           Reopen the log files
   resource_num user host num                                           Resource string of a session number
   restart                                                              Restart ejabberd
   restart_module host module                                           Stop an ejabberd module, reload code and start
   restore file                                                         Restore the database from backup file
   rooms_unused_destroy host days                                       Destroy the rooms that are unused for many days in host
   rooms_unused_list host days                                          List the rooms that are unused for many days in host
   rotate_log                                                           Rotate the log files
   send_direct_invitation name service password reason users            Send a direct invitation to several destinations
   send_message type from to subject body                               Send a message to a local or remote bare of full JID
   send_stanza from to stanza                                           Send a stanza; provide From JID and valid To JID
   send_stanza_c2s user host resource stanza                            Send a stanza as if sent from a c2s session
   set_last user host timestamp status                                  Set last activity information
   set_loglevel loglevel                                                Set the loglevel (0 to 5)
   set_master nodename                                                  Set master node of the clustered Mnesia tables
   set_nickname user host nickname                                      Set nickname in a user's vCard
   set_presence user host resource type show status priority            Set presence of a session
   set_room_affiliation name service jid affiliation                    Change an affiliation in a MUC room
   set_vcard user host name content                                     Set content in a vCard field
   set_vcard2 user host name subname content                            Set content in a vCard subfield
   set_vcard2_multi user host name subname contents                     *Set multiple contents in a vCard subfield
   srg_create group host name description display                       Create a Shared Roster Group
   srg_delete group host                                                Delete a Shared Roster Group
   srg_get_info group host                                              Get info of a Shared Roster Group
   srg_get_members group host                                           Get members of a Shared Roster Group
   srg_list host                                                        List the Shared Roster Groups in Host
   srg_user_add user host group grouphost                               Add the JID user@host to the Shared Roster Group
   srg_user_del user host group grouphost                               Delete this JID user@host from the Shared Roster Group
   stats name                                                           Get statistical value: registeredusers onlineusers onlineusersnode uptimeseconds processes
   stats_host name host                                                 Get statistical value for this host: registeredusers onlineusers
   status                                                               Get ejabberd status
   status_list status                                                   List of logged users with this status
   status_list_host host status                                         List of users logged in host with their statuses
   status_num status                                                    Number of logged users with this status
   status_num_host host status                                          Number of logged users with this status in host
   stop                                                                 Stop ejabberd
   stop_all_connections                                                 Stop all outgoing and incoming connections
   stop_kindly delay announcement                                       Inform users and rooms, wait, and stop the server
   subscribe_room user nick room nodes                                  Subscribe to a MUC conference
   unregister user host                                                 Unregister a user
   unsubscribe_room user room                                           Unsubscribe from a MUC conference
   update module                                                        Update the given module, or use the keyword: all
   update_list                                                          List modified modules that can be updated
   user_resources user server                                           List user's connected resources
   user_sessions_info user host                                         Get information about all sessions of a user