Register a user
===============
```shell
#!/bin/bash
for i in {1..100}
do
    ejabberdctl register nasterblue$i  192.168.99.100 password
done
```

Unregister a user
=================
```shell
#!/bin/bash
for i in {1..100}
do
    ejabberdctl unregister nasterblue$i  192.168.99.100
done
```

Create a MUC room name@service in host
======================================
```shell
#!/bin/bash
for i in {1..100}
do
    ejabberdctl create_room nasterblue_$i conference.192.168.99.100 192.168.99.100
done
```

Destroy a MUC room
==================
```shell
#!/bin/bash
for i in {1..100}
do
    ejabberdctl  destroy_room nasterblue_$i conference.192.168.99.100
done
```

Get the list of occupants of a MUC room
========================================
```shell
#!/bin/bash
for i in {1..100}
do
    ejabberdctl  get_room_occupants nasterblue_$i conference.192.168.99.100
done

ejabberdctl  get_room_occupants nasterblue_1 conference.192.168.99.100
```
