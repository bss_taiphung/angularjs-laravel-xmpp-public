/**
 * @ngdoc group module
 * @name group module
 * @description group module
 *
 *
 */

angular.module("ngBoilerplate.room", [
    'ui.router'
])
    .config(function config($stateProvider) {
        $stateProvider.state('room', {
            url: '/room',
            views: {
                "main": {
                    controller: 'Ctrl',
                    templateUrl: 'room/room.tpl.html'
                }
            },
            data: {
                pageTitle: 'Room'
            }
        });
    })
    .controller('Ctrl', function Ctrl($scope, $log, $q, $timeout, toastr) {


        //model

        $scope.chatModel = {
            isFetchedDisco: false,
            isFetchedHistory: false,
            rosters: [],
            requestSubscriptions: [],
            chatLogs: []
        };
        $scope.chatModel.requestSubscriptions = []; //list of request subscription


        //push to messages list
        $scope.pushToChatLogs = function (message) {
            if (message.body && message.type === $scope.message.type) {
                if (!$scope.chatModel.chatLogs.hasOwnProperty(message.from.bare)) {
                    $scope.chatModel.chatLogs[message.from.bare] = [];
                }

                $scope.chatModel.chatLogs[message.from.bare].push(message);
                $scope.$apply();
            }

        };

        //reset chat logs
        $scope.resetChatLogs = function () {
            $scope.chatModel.chatLogs = [];
        };

        //login
        $scope.login = function () {

            $scope.xmppClient = XMPP.createClient($scope.user);
            $scope.xmppClient.on('*', $scope.log);
            $scope.onClientEvent();


            //login success
            $scope.xmppClient.on('auth:success', function (data) {
                localStorage.setItem('user', JSON.stringify($scope.user));
                $scope.userState.logged = true;
                $scope.$apply();
            });

            $scope.xmppClient.on('auth:failed', function () {
                toastr.error('Your username or password is incorrect', 'Login failed');
            });


            //after logging
            $scope.xmppClient.on('session:started', function () {

                $scope.xmppClient.enableCarbons(function (err) {
                    if (err) {
                        console.log('Server does not support carbons');
                    }
                });

                $scope.xmppClient.getRoster(function (err, resp) {
                    if (resp.roster.items) {
                        $scope.chatModel.rosters = resp.roster.items;
                        $scope.$apply();
                    }

                    $scope.xmppClient.updateCaps();
                    $scope.xmppClient.sendPresence({
                        caps: $scope.xmppClient.disco.caps
                    });

                });

                $timeout(function () {
                    $scope.getDiscoItems();
                }, 1000);

            });
            $scope.xmppClient.connect();

        };

        //on event
        $scope.onClientEvent = function () {

            //get new message
            $scope.xmppClient.on('message', function (message) {
                if (message.error) {
                    toastr.info(message.error.text, null, {
                        closeButton: true,
                        closeHtml: '<button></button>',
                        onTap: function () {
                            var index = _.findIndex($scope.chatModel.rosters, function (roster) {
                                return roster.jid.bare === chat.from.bare;
                            });
                            $scope.chatWithRoster($scope.chatModel.rosters[index]);
                        }
                    });
                } else {
                    if (message.type === $scope.message.type) {
                        if (!message.chatState) {
                            $scope.pushToChatLogs(message);
                            $scope.userState.chatState = '';

                            $scope.$apply();
                            $scope.scrollToBottom();
                        }
                    }
                }

            });

            // //get sent messages
            // $scope.xmppClient.on('message:sent', function (message) {
            //     if (!message.chatState) {
            //         $scope.pushToChatLogs(message);
            //         $scope.message.body = '';
            //
            //         $scope.scrollToBottom();
            //     }
            // });


            //get current chat state
            //['active', 'composing', 'paused', 'inactive', 'gone']
            $scope.xmppClient.on('chat:state', function (state) {
                if (state.from.bare !== $scope.message.from) {
                    $scope.userState.chatState = state.chatState;
                    $scope.userState.message = state.from.bare + ' ' + state.chatState;
                    $scope.$apply();
                }else{
                    $scope.userState.chatState = '';
                }

            });

            //get all caps

            //on presence
            $scope.xmppClient.on('presence', function (presence) {
                //update presence of jid
                if (presence.from.bare === $scope.user.jid) {
                    return false;
                }

                if (presence.type === 'error') {
                    if ($scope.message.to) {
                        $scope.joinRoom($scope.message.to);
                    }

                } else {
                    $log.info(presence.from.bare + ' is ' + presence.type + ' now');
                    toastr.info(presence.from.bare + ' is ' + presence.type + ' now', 'Hello', {
                        closeButton: true,
                        closeHtml: '<button></button>'
                    });
                }
                $scope.$apply();
            });
        };

        $scope.setCurrentRoomChat = function (room) {
            $scope.message.to = room;
            $scope.joinRoom(room);
            $scope.scrollToBottom();
        };

        $scope.joinRoom = function (room) {
            $scope.xmppClient.joinRoom(room, $scope.message.nick, {
                status: 'This will be my nick ' + $scope.message.nick
            });

        };


        //fetch all history by all rooms
        $scope.fetchHistoryAllRooms = function () {
            $scope.chatModel.isFetchedHistory = false;
            var allPromises = [];
            _.each($scope.roomList, function (room) {
                $scope.joinRoom(room);
                allPromises.push($scope.fetchHistoryByRoom(room));
            });
            Promise.all(allPromises).then(function (response) {
                _.each(response, function (roomHistory, index) {
                    $scope.chatModel.chatLogs[$scope.roomList[index]] = roomHistory;
                });
                $scope.chatModel.isFetchedHistory = true;
                $scope.$apply();
            }, function (error) {
                $log.info(error);
            });
        };


        //fetch history by jid
        $scope.fetchHistoryByRoom = function (room) {
            //get chat history
            return new Promise(function (resolve, reject) {
                $scope.xmppClient.searchHistory({
                    to: room,
                    rsm: {
                        before: true
                    }
                }, function (error, response) {
                    if (error) {
                        reject(error);
                    } else {
                        var history = _.map(response.mamResult.items, function (mam) {
                            var message = mam.forwarded.message;
                            message.time = mam.forwarded.delay.stamp;
                            return message;
                        });
                        resolve(history);
                    }

                });
            });
        };


        $scope.getDiscoItems = function () {
            var room = 'conference.' + $scope.virtualHost;
            $scope.xmppClient.getDiscoItems(room, '', function (error, response) {
                if (response) {
                    var roomListTmp = _.reject(response.discoItems.items, function (item) {
                        // return item.jid.bare.indexOf('playorgo.ejabberd.com') === -1;
                        return item.jid.bare.indexOf('192.168.99.100') === -1;
                        // return true;
                    });
                    if (roomListTmp.length > 0) {
                        $scope.roomList = _.map(roomListTmp, function (item) {
                            return item.jid.bare;
                        });

                    } else {
                        $scope.roomList = [
                            // 'nasterblue@conference.' + $scope.virtualHost
                        ];

                        // for (var i = 0; i < 10; i++) {
                        //     $scope.roomList.push(
                        //         'nasterblue' + i + '@conference.' + $scope.virtualHost
                        //     );
                        // }
                        // $scope.roomList = ['9002@conference.socialoop.ejabberd.com'];

                    }

                    // $scope.roomList.push('003@conference.socialoop.ejabberd.com');

                    $scope.chatModel.isFetchedDisco = true;
                    $scope.fetchHistoryAllRooms();
                }

            });

            // $scope.getCurrentCaps();
            // $scope.getDiscoInfo(room);
            // $scope.discoverReservedNick(room);
        };

        $scope.getCurrentCaps = function () {
            $scope.xmppClient.getCurrentCaps();
        };

        $scope.getDiscoInfo = function (jid) {
            $scope.xmppClient.getDiscoInfo(jid, '', function (error, response) {
                $log.info(error, response);
            });
        };

        $scope.discoverReservedNick = function (jid) {
            $scope.xmppClient.discoverReservedNick(jid, function (error, response) {
                $log.info(error, response);
            });
        };


        //log out
        $scope.$parent.logout = function () {
            $scope.xmppClient.disconnect();
            localStorage.removeItem("user");
            $scope.userState.logged = false;
            $scope.resetChatLogs();
        };

        //scroll to bottom
        $scope.scrollToBottom = function () {
            $timeout(function () {
                try {
                    var container = angular.element(document.getElementById('chat-history'));
                    var scrollToElement = angular.element(document.getElementById('bottom'));
                    container.scrollTo(scrollToElement, 0, 1000);
                } catch (ex) {

                }
            }, 1000);
        };

        //log
        $scope.log = function (name, data) {
            if (name === 'auth:success') {
                localStorage.setItem('user', JSON.stringify($scope.user));
                $scope.userState.logged = true;
                $scope.$apply();
            }
        };


        //model
        if (localStorage.getItem('user')) {
            $scope.user = JSON.parse(localStorage.getItem('user'));
            $scope.login(); //auto login
        } else {
            $scope.user = {
                jid: 'nasterblue@' + $scope.virtualHost,
                password: "password",
                // boshURL: 'http://' + $scope.boshServiceWithPort + '/http-bind',
                // transports: ['bosh'],
                transports: ['websocket'],
                wsURL: 'ws://' + $scope.boshServiceWithPort + '/websocket'
            };
        }

        $scope.message = {
            from: $scope.user.jid,
            to: '',
            body: '',
            type: 'groupchat',
            nick: $scope.user.jid
        };


        //send message
        $scope.sendMessage = function () {
            if (!$scope.message.body) {
                //do not send empty message
                return false;
            }

            var msg = {
                type: $scope.message.type,
                to: $scope.message.to,
                from: $scope.message.from,
                body: $scope.message.body
            };

            $scope.xmppClient.sendMessage(msg);
            $scope.message.body = '';

        };


        $scope.setRoomAffiliation = function (room, jid) {
            $scope.xmppClient.setRoomAffiliation(room, jid, 'admin', '', function (err, res) {
                $log.info(err, res);
            });
        };

        // $scope.xmppClient.ban(room, jid, reason, [cb]);
        // $scope.xmppClient.changeNick(room, nick);
        // $scope.xmppClient.configureRoom(room, form, [cb]);
        // $scope.xmppClient.directInvite(room, sender, reason);
        // $scope.xmppClient.discoverReservedNick(room, [cb]);
        // $scope.xmppClient.getRoomConfig(jid, [cb]);
        // $scope.xmppClient.getRoomMembers(room, opts, [cb]);
        // $scope.xmppClient.getUniqueRoomName(jid, [cb]);
        // $scope.xmppClient.invite(room, opts);
        // $scope.xmppClient.joinRoom(room, nick, opts);
        // $scope.xmppClient.kick(room, nick, reason, [cb]);
        // $scope.xmppClient.leaveRoom(room, nick, opts);
        // $scope.xmppClient.requestRoomVoice(room);

        // $scope.xmppClient.setRoomRole(room, nick, role, reason, [cb]);
        // $scope.xmppClient.setSubject(room, subject);
        // $scope.xmppClient.addBookmark(bookmark, [cb]);
        // $scope.xmppClient.getBookmarks([cb]);
        // $scope.xmppClient.removeBookmark(jid, [cb]);
        // $scope.xmppClient.setBookmarks(opts, [cb]);
    });

