angular.module('directives', ['angularMoment', 'ngEmbed']);
angular.module('ngBoilerplate', [
    'templates-app',
    'templates-common',
    'ngAnimate',
    'ui.bootstrap',
    'duScroll',
    'picardy.fontawesome',
    'angularMoment',
    'toastr',
    'ngEmbed',
    'angular-spinkit',

    'directives',

    'ngBoilerplate.home',
    'ngBoilerplate.p2p',
    'ngBoilerplate.room',

    'ui.router'
])

    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/home');
    }])
    .config(['$logProvider', function ($logProvider) {
        $logProvider.debugEnabled(false);
    }])
    .config(['toastrConfig', function (toastrConfig) {
        angular.extend(toastrConfig, {
            autoDismiss: true,
            maxOpened: 1,
            newestOnTop: true,
            positionClass: 'toast-top-left',
            preventDuplicates: true,
            preventOpenDuplicates: true,
            target: 'body'
        });
    }])
    .constant('angularMomentConfig', {
        timezone: 'Asia/Ho_Chi_Minh'
    })
    .run(['amMoment', function run(amMoment) {

        amMoment.changeLocale('en');
        // moment.createFromInputFallback = function(config) {
        //     // unreliable string magic, or
        //     config._d = new Date(config._i);
        // };
    }])
    .value('duScrollDuration', 500)
    .value('duScrollOffset', 30)

    .controller('AppCtrl', ['$scope', function ($scope) {
        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (angular.isDefined(toState.data.pageTitle)) {
                $scope.pageTitle = toState.data.pageTitle + ' | BSS';
            }
        });


        //server localhost
        $scope.virtualHost = "192.168.99.100";
        $scope.boshService = "192.168.99.100";
        $scope.boshServiceWithPort = $scope.boshService + ":5280";


        //server 30
        // $scope.virtualHost = "nasterblue.ejabberd.com";
        // $scope.boshService = "web.beesightsoft.com";
        // $scope.boshServiceWithPort = $scope.boshService + ":5279";


        //server 128.199.94.240
        // $scope.virtualHost = "socialoop.ejabberd.com";
        // $scope.boshService = "128.199.94.240";
        // $scope.boshServiceWithPort = $scope.boshService + ":5280";
        //
        // server 128.199.94.240
        // $scope.virtualHost = "playorgo.ejabberd.com";
        // $scope.boshService = "128.199.94.240";
        // $scope.boshServiceWithPort = $scope.boshService + ":5277";


        $scope.isDemo = true;


        $scope.userState = {
            logged: false,
            chatState: '',
            message: ''
        };


        $scope.logout = function () {

        };

        $scope.embedOptions = {

            watchEmbedData: true,     // watch embed data and render on change

            sanitizeHtml: true,      // convert html to text

            fontSmiley: true,      // convert ascii smileys into font smileys
            emoji: true,      // convert emojis short names into images

            link: true,      // convert links into anchor tags
            linkTarget: '_self',   //_blank|_self|_parent|_top|framename

            pdf: {
                embed: true                 // show pdf viewer.
            },

            image: {
                embed: true                // toggle embedding image after link, supports gif|jpg|jpeg|tiff|png|svg|webp.
            },
            audio: {
                embed: true                 // toggle embedding audio player, supports wav|mp3|ogg
            },

            basicVideo: false,     // embed video player, supports ogv|webm|mp4
            gdevAuth: 'AIzaSyAz1L21geZxW-wpMTxo2rOCZUMG32YBJCY', //bss
            // gdevAuth: 'AIzaSyAQONTdSSaKwqB1X8i6dHgn9r_PtusDhq0',
            video: {
                'embed': false,
                'width': 800,
                'ytTheme': 'light',
                'details': true,
                thumbnailQuality: 'medium', // quality of the thumbnail low|medium|high
                autoPlay: false     // autoplay embedded videos
            },
            twitchtvEmbed: true,
            dailymotionEmbed: true,
            tedEmbed: true,
            dotsubEmbed: true,
            liveleakEmbed: true,
            ustreamEmbed: true,

            soundCloudEmbed: true,
            soundCloudOptions: {
                height: 160,
                themeColor: 'f50000',
                autoPlay: false,
                hideRelated: false,
                showComments: true,
                showUser: true,
                showReposts: false,
                visual: false,         // Show/hide the big preview image
                download: false          // Show/Hide download buttons
            },
            spotifyEmbed: true,

            tweetEmbed: true,        // toggle embedding tweets
            tweetOptions: {
                // The maximum width of a rendered Tweet in whole pixels. Must be between 220 and 550 inclusive.
                maxWidth: 550,
                // Toggle expanding links in Tweets to photo, video, or link previews.
                hideMedia: false,
                // When set to true or 1 a collapsed version of the previous Tweet in a conversation thread
                // will not be displayed when the requested Tweet is in reply to another Tweet.
                hideThread: false,
                // Specifies whether the embedded Tweet should be floated left, right, or center in
                // the page relative to the parent element.Valid values are left, right, center, and none.
                // Defaults to none, meaning no alignment styles are specified for the Tweet.
                align: 'none',
                // Request returned HTML and a rendered Tweet in the specified.
                // Supported Languages listed here (https://dev.twitter.com/web/overview/languages)
                lang: 'en'
            },

            code: {
                highlight: true,        // highlight code written in 100+ languages supported by highlight
                                        // requires highlight.js (https://highlightjs.org/) as dependency.
                lineNumbers: false        // display line numbers
            },
            codepenEmbed: true,
            codepenHeight: 300,
            jsfiddleEmbed: true,
            jsfiddleHeight: 300,
            jsbinEmbed: true,
            jsbinHeight: 300,
            plunkerEmbed: true,
            githubgistEmbed: true,
            ideoneEmbed: true,
            ideoneHeight: 300
        };


    }]);
