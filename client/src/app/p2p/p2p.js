/**
 * @ngdoc user module
 * @name user module
 * @description user module
 *
 *
 */
angular.module("ngBoilerplate.p2p", [
    'ui.router'
])
    .config(function config($stateProvider) {
        $stateProvider.state('p2p', {
            url: '/p2p',
            views: {
                "main": {
                    controller: 'UserCtrl',
                    templateUrl: 'p2p/p2p.tpl.html'
                }
            },
            data: {
                pageTitle: 'p2p'
            }
        });
    })
    .controller('UserCtrl', function UserCtrl($scope, $log, $timeout, toastr) {

        //model

        $scope.chatModel = {
            isFetchedRoster: false,
            isFetchedHistory: false,
            rosters: [],
            requestSubscriptions: [],
            chatLogs: []
        };
        $scope.chatModel.requestSubscriptions = []; //list of request subscription


        //push to messages list
        $scope.pushToChatLogs = function (message) {
            if (message.body) {
                $scope.chatModel.chatLogs[message.from.bare].push(message);
            }
        };

        //push to messages list
        $scope.pushToChatLogsThatIJustSent = function (message) {
            if (message.body) {
                if (!$scope.chatModel.chatLogs[message.to.bare]) {
                    $scope.chatModel.chatLogs[message.to.bare] = [];
                }
                $scope.chatModel.chatLogs[message.to.bare].push(message);
            }
        };


        //login
        $scope.login = function () {

            $scope.xmppClient = XMPP.createClient($scope.user);
            $scope.xmppClient.on('*', $scope.log);
            $scope.onClientEvent();


            //login success
            $scope.xmppClient.on('auth:success', function (data) {
                $log.info('auth:success', data);
                localStorage.setItem('user', JSON.stringify($scope.user));
                $scope.userState.logged = true;
                $scope.$apply();
            });

            $scope.xmppClient.on('auth:failed', function () {
                $log.info('auth:failed');
                toastr.error('Your username or password is incorrect', 'Login failed');
            });


            //after logging
            $scope.xmppClient.on('session:started', function () {


                //test send msg begin
                //
                // setTimeout(function () {
                //     var jid = '';
                //     if ($scope.user.jid === 'nasterblue@' + $scope.virtualHost) {
                //         jid = 'bss1@' + $scope.virtualHost;
                //     } else {
                //         jid = 'nasterblue@' + $scope.virtualHost;
                //     }
                //     $scope.xmppClient.sendMessage({
                //         to: jid,
                //         body: "Hey Thai!" + new Date().toDateString()
                //     });
                //     $scope.fetchHistoryByJid(jid).then(function (onsuceed) {
                //         console.log(onsuceed);
                //     }, function (onrejected) {
                //         console.log(onrejected);
                //     });
                // }, 2000);
                //test send msg end


                $scope.xmppClient.enableCarbons(function (err) {
                    if (err) {
                        console.log('Server does not support carbons');
                    }
                });
                $scope.fetchLatestRosters();

                $timeout(function () {
                    $scope.fetchHistoryAllRosters();
                }, 1000);
            });
            $scope.xmppClient.connect();

        };

        //on event
        $scope.onClientEvent = function () {

            //on chat
            $scope.xmppClient.on('chat', function (chat) {

                //show notify when other user chat to you
                if (chat.from.bare !== $scope.message.to) {
                    toastr.info(chat.body, 'New message from ' + chat.from.local, {
                        closeButton: true,
                        closeHtml: '<button></button>',
                        onTap: function () {
                            var index = _.findIndex($scope.chatModel.rosters, function (roster) {
                                return roster.jid.bare === chat.from.bare;
                            });
                            $scope.setCurrentRoster($scope.chatModel.rosters[index]);
                        }
                    });
                }
            });


            //get new message
            $scope.xmppClient.on('message', function (message) {
                //only append IM to history to current chat user
                if (!message.chatState) {
                    $scope.pushToChatLogs(message);
                    $scope.userState.chatState = '';

                    $scope.$apply();
                    $scope.scrollToBottom();
                }

            });

            //get sent messages
            $scope.xmppClient.on('message:sent', function (message) {
                if (!message.chatState) {
                    $scope.pushToChatLogsThatIJustSent(message);
                    $scope.message.body = '';

                    $scope.scrollToBottom();
                }
            });


            //get current chat state
            //['active', 'composing', 'paused', 'inactive', 'gone']
            $scope.xmppClient.on('chat:state', function (state) {
                if (state.from.bare === $scope.message.to) {
                    $log.info('chat:state', state);
                    $scope.userState.chatState = state.chatState;
                    $scope.userState.message = state.from.bare + ' ' + state.chatState;
                    $scope.$apply();
                } else {
                    $scope.userState.chatState = '';
                }
            });


            //get all caps

            //on presence
            $scope.xmppClient.on('presence', function (presence) {
                //update presence of jid
                if (presence.from.bare === $scope.user.jid) {
                    return false;
                }
                $log.info('presence', presence);

                var index = _.findIndex($scope.chatModel.rosters, function (roster) {
                    return roster.jid.bare === presence.from.bare;
                });


                $scope.chatModel.rosters[index].presence = presence;

                toastr.info($scope.chatModel.rosters[index].jid.local + ' is ' + presence.type + ' now', 'Hello', {
                    closeButton: true,
                    closeHtml: '<button></button>'
                });

                $scope.$apply();
            });

            //on subscribe => jid received a subscription request
            $scope.xmppClient.on('subscribe', function (subscribe) {
                $log.info('subscribe', subscribe);
                toastr.info(subscribe.from.local + ' want to be your friend', 'Hello', {
                    closeButton: true,
                    closeHtml: '<button></button>'
                });

                $scope.chatModel.requestSubscriptions.push(subscribe);
                $scope.$apply();
            });

            //on subscribed => subscription request was be accepted
            $scope.xmppClient.on('subscribed', function (subscribed) {
                $log.info('subscribed', subscribed);

                toastr.info(subscribed.from.local + ' is  your friend now', 'Hello', {
                    closeButton: true,
                    closeHtml: '<button></button>'
                });


                //fetch latest roster
                $scope.fetchLatestRosters();
            });


            //on unsubscribed
            $scope.xmppClient.on('unsubscribed', function (unsubscribed) {
                $log.info('unsubscribed', unsubscribed);
                //fetch latest roster
                $scope.fetchLatestRosters();
            });

            //on unsubscribe
            $scope.xmppClient.on('unsubscribe', function (unsubscribe) {
                $log.info('unsubscribe', unsubscribe);
                //fetch latest roster
                $scope.fetchLatestRosters();
            });

            //on block
            $scope.xmppClient.on('block', function (block) {
                $log.info('block', block);
            });

            //on unblock
            $scope.xmppClient.on('unblock', function (unblock) {
                $log.info('unblock', unblock);
            });

            //on connected
            $scope.xmppClient.on('connected', function (connected) {
                $log.info('connected', connected);
            });

            //on disconnected
            $scope.xmppClient.on('disconnected', function (disconnected) {
                $log.info('connected', disconnected);
            });


            //on roster:update
            $scope.xmppClient.on('roster:update', function (roster) {
                $log.info(roster);
                $scope.fetchLatestRosters();
            });

        };

        //fetch attentions
        $scope.fetchAttentions = function () {
            for (var i = 0; i < $scope.chatModel.rosters.length; i++) {
                $scope.getAttention($scope.chatModel.rosters[i].jid.bare);
            }
        };

        //get attention
        $scope.getAttention = function (jid) {
            $scope.xmppClient.getAttention(jid);
        };

        //log out
        $scope.$parent.logout = function () {
            $scope.xmppClient.disconnect();
            localStorage.removeItem("user");
            $scope.userState.logged = false;
        };

        //subscribe jid
        $scope.jidFriend = 'bss1@' + $scope.virtualHost;
        $scope.subscribe = function (jid) {
            $scope.xmppClient.subscribe(jid);
        };

        //unsubscribe jid
        $scope.unsubscribe = function (jid) {
            $scope.xmppClient.unsubscribe(jid, function (err, res) {
                $log.info(err, res);
            });
        };

        //unblock jid
        $scope.unblock = function (jid) {
            $scope.xmppClient.unblock(jid, function (err, res) {
                $log.info(err, res);
            });
        };

        //block jid
        $scope.block = function (jid) {
            $scope.xmppClient.block(jid, function (err, res) {
                $log.info(err, res);
                $scope.updateRequestSubscriptions(jid);
            });
        };

        //denySubscription roster jid
        $scope.denySubscription = function (jid) {
            $scope.xmppClient.denySubscription(jid);
            $scope.updateRequestSubscriptions(jid);
        };

        //remove roster jid
        $scope.removeRosterItem = function (jid) {
            $scope.xmppClient.removeRosterItem(jid);
            //fetch latest roster
            $scope.fetchLatestRosters();
        };

        //accept subscription
        $scope.acceptSubscription = function (bare) {
            $scope.xmppClient.acceptSubscription(bare);

            $scope.updateRequestSubscriptions(bare);

            //fetch latest roster
            $scope.fetchLatestRosters();
        };


        //update request subscription list
        $scope.updateRequestSubscriptions = function (bare) {

            var index = _.findIndex($scope.chatModel.requestSubscriptions, function (subscription) {
                return subscription.from.bare === bare;
            });
            $scope.chatModel.requestSubscriptions.splice(index, 1);

        };

        //fetch latest rosters
        $scope.fetchLatestRosters = function () {
            //fetch latest roster
            $scope.chatModel.rosters = [];
            $scope.xmppClient.getRoster(function (err, resp) {
                if (resp.roster.items) {
                    $scope.chatModel.rosters = resp.roster.items;
                    $scope.$apply();
                }
                $scope.chatModel.isFetchedRoster = true;

                $scope.xmppClient.updateCaps();
                $scope.xmppClient.sendPresence({
                    caps: $scope.xmppClient.disco.caps
                });


            });
        };

        //fetch all history by all rooms
        $scope.fetchHistoryAllRosters = function () {
            var allPromises = [];
            _.each($scope.chatModel.rosters, function (roster) {
                allPromises.push($scope.fetchHistoryByJid(roster.jid.bare));
            });
            // allPromises.push($scope.fetchHistoryByJid($scope.message.from));
            Promise.all(allPromises).then(function (response) {
                _.each(response, function (p2pHistory, index) {
                    $scope.chatModel.chatLogs[$scope.chatModel.rosters[index].jid.bare] = p2pHistory;
                });
                $scope.chatModel.isFetchedHistory = true;
            }, function (error) {
                $log.info(error);
            });
        };


        //fetch history by jid
        $scope.fetchHistoryByJid = function (jid) {
            //get chat history
            return new Promise(function (resolve, reject) {
                $scope.xmppClient.searchHistory({
                    'with': jid
                }, function (error, response) {
                    if (error) {
                        reject(error);
                    } else {
                        var history = _.map(response.mamResult.items, function (mam) {
                            var message = mam.forwarded.message;
                            message.time = mam.forwarded.delay.stamp;
                            return message;
                        });
                        resolve(history);
                    }

                });
            });
        };


        //chat with roster
        $scope.setCurrentRoster = function (roster) {

            var jid = roster.jid.bare;

            $scope.message.from = $scope.user.jid;
            $scope.message.to = jid;
            $scope.message.roster = roster;

            $scope.chatState = {
                from: $scope.user.jid,
                to: jid
            };

            $scope.scrollToBottom();

        };

        //scroll to bottom
        $scope.scrollToBottom = function () {
            $timeout(function () {
                try {
                    var container = angular.element(document.getElementById('chat-history'));
                    var scrollToElement = angular.element(document.getElementById('bottom'));
                    container.scrollTo(scrollToElement, 0, 1000);
                } catch (ex) {

                }
            }, 1000);
        };

        //send message
        $scope.sendMessage = function () {
            if (!$scope.message.body) {
                //do not send empty message
                return false;
            }
            $scope.xmppClient.sendMessage($scope.message);
        };


        //log
        $scope.log = function (name, data) {
            if (name == 'auth:success') {
                localStorage.setItem('user', JSON.stringify($scope.user));
                $scope.userState.logged = true;
                $scope.$apply();
            }
            $log.info(name, data);
        };


        //model
        if (localStorage.getItem('user')) {
            $scope.user = JSON.parse(localStorage.getItem('user'));
            $scope.login(); //auto login
        } else {
            $scope.user = {
                jid: 'nasterblue@' + $scope.virtualHost,
                password: "password",
                // boshURL: 'http://' + $scope.boshService + '/http-bind',
                // transports: ['bosh'],
                transports: ['websocket'],
                // wsURL: 'ws://' + $scope.boshServiceWithPort + '/ws-xmpp'
                wsURL: 'ws://' + $scope.boshServiceWithPort + '/websocket'
            };
        }

        $scope.message = {
            from: $scope.user.jid,
            to: '',
            body: ''
        };
    });
