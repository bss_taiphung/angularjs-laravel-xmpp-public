/**
 * Created by nasterblue on 5/30/16.
 */
angular.module('directives')

    .directive('chatbtn', function ($timeout, $log) {
        return {
            restrict: 'AE',
            templateUrl: 'directives/chatbtn/chatbtn.tpl.html',
            scope: {
                xmppClient: '=',
                message: '=',
                sendMessage: '&'
            },
            link: function (scope, $element, $attrs) {

                scope.onTyping = function (chatState) {
                    if (scope.message.type !== "groupchat") {
                        var msg = {
                            type: 'chat',
                            to: scope.message.to,
                            from: scope.message.from,
                            chatState: chatState
                        };
                        scope.xmppClient.sendMessage(msg);
                    } else {

                        try {
                            // mod_muc:
                            //     access_admin: all


                            scope.xmppClient.getRoomMembers(scope.message.to, {items: [{role: 'moderator'}]}, function (err, res) {
                                if (res && res.mucAdmin) {
                                    var uniqueList = _.uniq(res.mucAdmin.items, function (item, key, a) {
                                        return item.jid.bare;
                                    });
                                    for (var x in uniqueList) {
                                        if (uniqueList[x].jid && uniqueList[x].jid.bare && uniqueList[x].jid.bare !== scope.message.from) {
                                            var toJID = uniqueList[x].jid.bare;
                                            var msg = {
                                                type: 'chat',
                                                to: toJID,
                                                from: scope.message.from,
                                                chatState: chatState
                                            };
                                            scope.xmppClient.sendMessage(msg);
                                        }

                                    }
                                    $log.info(err, res);
                                }

                            });

                        } catch (error) {
                            $log.info(error);
                        }


                    }


                };

                scope.typing = false;
                $element.on('keyup', function () {
                    if (scope.typing) {
                        if (scope.pendingPromise) {
                            $timeout.cancel(scope.pendingPromise);
                        }
                        scope.pendingPromise = $timeout(function () {
                            scope.typing = false;
                            scope.$apply(function () {
                                scope.onTyping('paused');
                            });
                        }, 2000);
                    }
                });

                $element.on('keydown', function (e) {
                    if (!scope.typing) {
                        scope.typing = true;
                        scope.onTyping('composing');
                    }
                });
            }
        };
    })

;
