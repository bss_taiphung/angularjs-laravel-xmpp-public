/**
 * Created by nasterblue on 5/30/16.
 */
angular.module('directives')

    .directive('imp2p', function ($compile, $http, $templateCache) {
        return {
            restrict: 'E',
            scope: {
                localUser: '=',
                chat: '=',
                embedOptions : '='
            },
            templateUrl: 'directives/imp2p/imp2p.tpl.html'
        };
    });
